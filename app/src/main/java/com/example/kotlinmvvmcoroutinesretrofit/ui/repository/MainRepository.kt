package com.example.kotlinmvvmcoroutinesretrofit.ui.repository

import com.example.kotlinmvvmcoroutinesretrofit.model.PhotoModel
import retrofit2.Call

interface MainRepository {
    suspend fun getPhotos():List<PhotoModel>
}