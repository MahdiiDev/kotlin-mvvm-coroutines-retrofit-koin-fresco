package com.example.kotlinmvvmcoroutinesretrofit.utils

import com.example.kotlinmvvmcoroutinesretrofit.custom.MyImageView

interface ImageLoading {
    fun loadImage(view:MyImageView,url:String)
}