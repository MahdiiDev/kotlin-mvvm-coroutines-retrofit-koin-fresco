package com.example.kotlinmvvmcoroutinesretrofit.ui.dataSource

import com.example.kotlinmvvmcoroutinesretrofit.model.PhotoModel
import com.example.kotlinmvvmcoroutinesretrofit.retrofit.ApiService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call

class RemoteMainDataSource(val apiService: ApiService):MainDataSource {
    override suspend fun getPhotos(): List<PhotoModel> = apiService.getPhotos()
}