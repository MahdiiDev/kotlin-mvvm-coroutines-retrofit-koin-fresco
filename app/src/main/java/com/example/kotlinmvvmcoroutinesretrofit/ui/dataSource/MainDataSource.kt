package com.example.kotlinmvvmcoroutinesretrofit.ui.dataSource

import com.example.kotlinmvvmcoroutinesretrofit.model.PhotoModel
import retrofit2.Call

interface MainDataSource {
    suspend fun getPhotos():List<PhotoModel>
}