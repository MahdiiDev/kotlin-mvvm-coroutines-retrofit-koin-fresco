package com.example.kotlinmvvmcoroutinesretrofit.retrofit

import com.example.kotlinmvvmcoroutinesretrofit.model.PhotoModel
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiService {

    @GET("photos")
    suspend fun getPhotos():List<PhotoModel>
}

fun getClient():ApiService{
    var retrofit=Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

        return retrofit.create(ApiService::class.java)
}