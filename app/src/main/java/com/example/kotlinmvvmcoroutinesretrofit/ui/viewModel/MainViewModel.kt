package com.example.kotlinmvvmcoroutinesretrofit.ui.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kotlinmvvmcoroutinesretrofit.model.PhotoModel
import com.example.kotlinmvvmcoroutinesretrofit.ui.repository.MainRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel(val mainRepository: MainRepository):ViewModel() {
        val photoLiveData=MutableLiveData<List<PhotoModel>>()
    init {
        viewModelScope.launch(Dispatchers.Main) {
          val photos=mainRepository.getPhotos()
            photoLiveData.postValue(photos)
        }
    }
}