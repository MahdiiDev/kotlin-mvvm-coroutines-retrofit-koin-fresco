package com.example.kotlinmvvmcoroutinesretrofit.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinmvvmcoroutinesretrofit.R
import com.example.kotlinmvvmcoroutinesretrofit.ui.adapter.MainAdapter
import com.example.kotlinmvvmcoroutinesretrofit.ui.viewModel.MainViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class MainActivity : AppCompatActivity() {
    val viewModel:MainViewModel by viewModel()
    lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViews()
        viewModel.photoLiveData.observe(this) {
            val adapter:MainAdapter by inject { parametersOf(it) }
            recyclerView.adapter=adapter
        }
    }

    private fun setupViews() {
        recyclerView=findViewById(R.id.rv_main)
        recyclerView.layoutManager= GridLayoutManager(this,2)

    }
}