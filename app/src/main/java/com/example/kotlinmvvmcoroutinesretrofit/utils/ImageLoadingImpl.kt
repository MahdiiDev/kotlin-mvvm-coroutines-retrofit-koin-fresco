package com.example.kotlinmvvmcoroutinesretrofit.utils

import com.example.kotlinmvvmcoroutinesretrofit.custom.MyImageView
import com.facebook.drawee.view.SimpleDraweeView

class ImageLoadingImpl:ImageLoading {
    override fun loadImage(view: MyImageView, url: String) {
        if (view is SimpleDraweeView){
            view.setImageURI(url)
        }
    }
}