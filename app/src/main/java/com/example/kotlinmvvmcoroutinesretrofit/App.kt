package com.example.kotlinmvvmcoroutinesretrofit

import android.app.Application
import com.example.kotlinmvvmcoroutinesretrofit.model.PhotoModel
import com.example.kotlinmvvmcoroutinesretrofit.retrofit.getClient
import com.example.kotlinmvvmcoroutinesretrofit.ui.adapter.MainAdapter
import com.example.kotlinmvvmcoroutinesretrofit.ui.dataSource.RemoteMainDataSource
import com.example.kotlinmvvmcoroutinesretrofit.ui.repository.MainRepository
import com.example.kotlinmvvmcoroutinesretrofit.ui.repository.MainRepositoryImpl
import com.example.kotlinmvvmcoroutinesretrofit.ui.viewModel.MainViewModel
import com.example.kotlinmvvmcoroutinesretrofit.utils.ImageLoading
import com.example.kotlinmvvmcoroutinesretrofit.utils.ImageLoadingImpl
import com.facebook.drawee.backends.pipeline.Fresco
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
        val modules= module {
            single { getClient() }
            factory {(listPhoto:List<PhotoModel>)-> MainAdapter(listPhoto,get()) }
            single<ImageLoading> { ImageLoadingImpl() }
            factory<MainRepository> { MainRepositoryImpl(RemoteMainDataSource(get())) }
            single { MainViewModel(get()) }
        }
        startKoin {
            androidContext(this@App)
            modules(modules)
        }
    }
}