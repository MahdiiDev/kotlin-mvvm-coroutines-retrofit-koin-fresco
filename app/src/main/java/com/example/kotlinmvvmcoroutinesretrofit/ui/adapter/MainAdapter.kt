package com.example.kotlinmvvmcoroutinesretrofit.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinmvvmcoroutinesretrofit.R
import com.example.kotlinmvvmcoroutinesretrofit.custom.MyImageView
import com.example.kotlinmvvmcoroutinesretrofit.model.PhotoModel
import com.example.kotlinmvvmcoroutinesretrofit.utils.ImageLoading

class MainAdapter(var list:List<PhotoModel>,var imageLoading: ImageLoading): RecyclerView.Adapter<MainAdapter.MainViewHolder>() {
    class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val txtTitle=itemView.findViewById<TextView>(R.id.txt_photoItem)
        val img=itemView.findViewById<MyImageView>(R.id.img_photoItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.photo_item,parent,false))
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.txtTitle.text=list[position].title
        imageLoading.loadImage(holder.img,list[position].thumbnailUrl)
    }

    override fun getItemCount(): Int = list.size
}