package com.example.kotlinmvvmcoroutinesretrofit.ui.repository

import com.example.kotlinmvvmcoroutinesretrofit.model.PhotoModel
import com.example.kotlinmvvmcoroutinesretrofit.ui.dataSource.MainDataSource
import retrofit2.Call

class MainRepositoryImpl(val remoteMainDataSource: MainDataSource):MainRepository {
    override suspend fun getPhotos(): List<PhotoModel> = remoteMainDataSource.getPhotos()
}